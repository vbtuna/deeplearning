# About Lego-Faces
## Content
This dataset contains 25 images of LEGO minifigure Faces and taken from Kaggle datasets
for review dataset, please visit [Kaggle link of dataset](https://www.kaggle.com/datasets/ihelon/lego-minifigure-faces)

# About Project and Result
An AutoEncoder is a type of artificial Neural Network used to learn efficient codings of unlabeled data
(Unsupervised Learning) [Wikipedia](https://en.wikipedia.org/wiki/Autoencoder)
It consists of two neural and models as a typical autoencoder encoder and decoder.
encoder is the model that learns to reduce the size of the data to code,
decoder is the model that converts the reduced code into real data

In this project, the encoder model consists of 4 layers and each layer consists of 300,512,450,300 perceptron 
respectively. The activation function used is the Tanh activation function in all layers
The decoder model also contains 4 layers, and 512,450,300,1024 perceptron is used, 
the activation function used is the sigmoid activation function

In the AutoEncoder model created by combining these two models, 
Adam was used as Optimization Function during training. The learning speed is 0.0001
With 400 epochs and 4 batch_size, 0.5926 loss value and 0.5951 val_loss value were obtained

# Lego-Yüzleri hakkında
## Proje Hakkında ve Sonuç
bu veri kümesi Kaggle verisetlerinden alınmış olup 25 LEGO minifigür yüz görseli içermektedir
veri kümesini incelemek için ziyaret edin [veri kümesinin Kaggle linki](https://www.kaggle.com/datasets/ihelon/lego-minifigure-faces)

Tipik bir Oto-kodlayıcı Encoder-kodlayıcı- ve Decoder-kod çözücü- olarak iki modelden oluşur.
Encoder, verinin boyutunu azaltacak kodu öğrenirken,
Decoder, kodlanmış verinin gerçek veriye dönüştürmeyi öğrenir.

Bu projede encoder 4 katman içermekte olup katmanlar sırasıyla 300,512,450,300 perceptrondan-yapay nöral ağ hücresi-
içermektedir, encoder katmanlarının hepsinde Relu aktivasyon fonksiyonu kullanılmıştır
decoder modelide yine 4 katman içermekte olup her katman sırasıyla 512,,450,300,1024 perceptron içermektedir
tüm decoder katmanlarında aktivasyon fonksiyonu Sigmoid aktivasyon fonksiyonudur.

bu iki modelin birleştirilmesiyle oluşturulan AutoEncoder modelinde eğitim sırasında Optimizasyon Fonksiyonu 
olarak Adam kullanılmıştır öğrenme hızı 0.0001 dir

400 epochs ve 4 batch_size ile 0.5926 loss değeri, 0.5951 val_loss değeri elde edildi
